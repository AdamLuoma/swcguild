function nameFieldCheck() {
	if (document.getElementById('nameField').value == "") {
		alert('Please fill in name');
		return false;
	}
	else {
		return true;
	}
}

function emailPhoneCheck() {
	var emailField = document.getElementById('emailField').value;
	var phoneField = document.getElementById('phoneField').value;
	if (emailField == "" && phoneField == "") {
		alert('Please fill in Email or Phone');
		return false;
	}
	else {
		return true;
	}
}

function selectOther() {
	var selection = document.getElementById('selection').value;
	var addInfo = document.getElementById('textarea').value;
	if (selection == "Other" && addInfo == "") {
		alert('Please fill out Additional Info');
		return false;
	}
	else {
		return true;
	}
}

function checkDays() {
	var avail = document.getElementsByName('avail[]');
	var hasChecked = false;
	for (var i = 0; i < avail.length; i++) {
		if (avail[i].checked){
			hasChecked = true;
			break;
		}
	}
	if (hasChecked == false) {
		alert('Please Select an Available Day');
		return false;
	}
	return true;
}

