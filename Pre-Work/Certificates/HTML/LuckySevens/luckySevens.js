
function hideResults() {
		document.getElementById("results").style.display="none";
	}

function rollDice() {
	var startingBet = document.getElementById('startingBet').value;
	var bet = startingBet;
	var d1 = Math.floor(Math.random() * 6) + 1;
	var d2 = Math.floor(Math.random() * 6) + 1;
	var diceTotal = d1 + d2;
	var betsArray = [];

		while (bet > 0) {
			if(diceTotal != 7) {
				bet -= 1
			}
			else {
				bet += 4
			}
			betsArray.push(bet);
			var d1 = Math.floor(Math.random() * 6) + 1;
			var d2 = Math.floor(Math.random() * 6) + 1;
			var diceTotal = d1 + d2;
		}

	var rollCounter = betsArray.length;
	var highestAmount = Math.max.apply(Math, betsArray);
	var highestPosition = betsArray.indexOf(highestAmount);
	var rollsFromHighest = rollCounter - highestPosition;

function showResults() {
	document.getElementById("results").style.display ="inline";
	document.getElementById("playButton").innerHTML = "Play Again";
	document.getElementById("bet").innerHTML="$" + startingBet +".00";
	document.getElementById("totalRolls").innerHTML = rollCounter;
	document.getElementById("highestAmount").innerHTML = "$" + highestAmount + ".00";
	document.getElementById("rollCountHighest").innerHTML = rollsFromHighest;
	};
	showResults();
}